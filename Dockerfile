# build stage
FROM node:21.0.0-alpine  AS build-stage
WORKDIR /src
COPY ./src .

# production stage
FROM nginx AS production-stage
COPY --from=build-stage /src /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

